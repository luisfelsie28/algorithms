public class Arrays {
    static int findSmallest (int[] array){
        int smallestNumber = array[0];
        for (int number : array) {
            smallestNumber = (number < smallestNumber) ? number : smallestNumber;
        }
        return smallestNumber;
    }
}