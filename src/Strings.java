
public class Strings {
    public static boolean matchAbc(String phrase, String part){
        if (part.length() > phrase.length()) {
            return false;
        }

        // Extract the substring from str that has the same length as ending, starting from the end
        String extractedEnding = phrase.substring(phrase.length() - part.length());

        // Compare the extracted ending with the provided ending
        return extractedEnding.equals(part);
    }
}
